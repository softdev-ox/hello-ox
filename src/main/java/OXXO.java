
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OMEN
 */
public class OXXO {

    static Scanner kb = new Scanner(System.in);
    static char winner = '-';
    static boolean isFinish = false;
    static int Row, Col;
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to Ox Game");
    }

    static void showTable() {
        System.out.println(" 123");

        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            Row = kb.nextInt() - 1;
            Col = kb.nextInt() - 1;

            if (table[Row][Col] == '-') {
                table[Row][Col] = player;
                break;
            }
            System.out.println("Error: Table at Row and Col is not Empty!!!");
        }
    }
    static void checkCol(){
        for(int Row = 0; Row < 3; Row++){
            if(table[Row][Col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
    static void checkRow(){
        for(int Col = 0; Col < 3; Col++){
            if(table[Row][Col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
    static void checkX(){
        
    }
    static void checkDeaw(){
        
    }
    static void checkWin() {
        checkRow();
        checkCol();
    }
    

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if(winner == '-'){
            System.out.println("Draw");
        }else{
            System.out.println(winner + " Win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    public static void main(String[] args) {

        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
